# Nina

Learning to build webapps with Rust
by building a basic CMS

#### What it does so far
Well, if the db container isn't running, it won't start.

So first you need to start the database with `docker start ninadb`.

And now we're getting an error because of some reference sharing...

### Item
An item will be represented by:

- a name (string)

- a description (string)


This is a rather simple schema,
which can later be complexified into something
that could be used in e-commerce by adding
additional images, videos, etc, as well
as a price.

### Database
We'll be using the same old postgres we always use,
with the `sqlx` library.

We'll put all the configuration information in a
`.env` file, which we'll read, parse, etc using the standard
library and maybe `serde`.

### status
Added delete, update.
Possibly try a 'get item by name' function.
