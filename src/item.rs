use serde::Deserialize;
use sqlx::FromRow;
use sqlx::PgPool;
use std::fmt;
use std::io;

#[derive(Debug, Deserialize)]
pub struct ItemForm {
    pub name: String,
    pub description: String,
}

#[derive(Debug, Deserialize)]
pub struct Item {
    pub id: i32,
    pub name: String,
    pub description: String,
}

impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "\n{}:\n{}\n", self.name, self.description)
    }
}

#[derive(Debug, FromRow)]
pub struct Items {
    pub list: Vec<Item>,
}

impl fmt::Display for Items {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for item in self.list.iter() {
            writeln!(f, "{}", item)?;
        }
        write!(f, "")
    }
}

impl Iterator for Items {
    type Item = Item;

    fn next(&mut self) -> Option<Self::Item> {
        // TODO check that this actually works
        None
    }
}

pub async fn add_item(item: ItemForm, pool: &PgPool) -> Result<(), io::Error> {
    sqlx::query!(
        "insert into items (name, description) values ($1,$2);",
        item.name,
        item.description
    )
    .execute(pool)
    .await
    .unwrap();
    Ok(())
}

pub async fn list_items(pool: &PgPool) -> Result<Items, io::Error> {
    let rows = sqlx::query!("select id, name, description  from items;")
        .fetch_all(pool)
        .await
        .unwrap();
    println!("rows: {:?}", rows);
    let mut out = Items { list: vec![] };
    for r in rows {
        out.list.push(Item {
            id: r.id,
            name: r.name.unwrap(),
            description: r.description.unwrap(),
        })
    }
    Ok(out)
}

// get_item returns the first item matching name.
pub async fn get_item_by_name(name: String, pool: &PgPool) -> Result<Item, io::Error> {
    let row = sqlx::query!(
        "select id, name, description from items where name = $1",
        name
    )
    .fetch_one(pool)
    .await
    .unwrap();
    Ok(Item {
        id: row.id,
        name,
        description: row.description.unwrap(),
    })
}

pub async fn get_item_by_id(id: i32, pool: &PgPool) -> Result<Item, io::Error> {
    let row = sqlx::query!("select name, description from items where id = $1", id)
        .fetch_one(pool)
        .await
        .unwrap();
    Ok(Item {
        id,
        name: row.name.unwrap(),
        description: row.description.unwrap(),
    })
}

// delete_item deletes the item with the given ID
pub async fn delete_item(id: i32, pool: &PgPool) -> Result<(), io::Error> {
    sqlx::query!("delete from items where id = $1", id)
        .execute(pool)
        .await
        .unwrap();
    Ok(())
}
