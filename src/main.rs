use askama::Template;
use axum::{
    extract::{Form, Path, State},
    response::{Html, IntoResponse},
    routing::get,
    Router,
};
use axum_macros;
use sqlx::postgres::{PgPool, PgPoolOptions};
use std::process::Command;
use std::{env, net::SocketAddr};

mod config;
mod item;

use item::ItemForm;

#[tokio::main]
async fn main() {
    println!("Hello, world!\nIt's me, Nina!");

    println!("Starting database");
    Command::new("sh")
        .arg("-c")
        .arg("docker start ninadb")
        .output()
        .expect("failed to start database");

    let database_url = "postgres://postgres:toto@172.17.0.2:5432"; // TODO read from config
    env::set_var("DATABASE_URL", database_url);

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await
        .expect("Failed to create pool");

    let app = Router::new()
        .route("/", get(root))
        .route("/items", get(get_items).post(add_item))
        .route("/:id", get(get_item).delete(delete_item))
        .with_state(pool);
    let address = SocketAddr::from(([127, 0, 0, 1], 8080));

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(Template)]
#[template(path = "first.html")]
struct FirstTemplate {}

async fn root() -> impl IntoResponse {
    println!("got request");
    let template = FirstTemplate {};
    Html(template.render().unwrap())
}

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {
    items: item::Items, // Vec<Item> does not implement fmt::Display
                        // so we need to first make it a string
                        // or derive the trait? Is that even possible?
}

async fn get_items(State(pool): State<PgPool>) -> impl IntoResponse {
    let items = item::list_items(&pool).await.unwrap();
    let template = IndexTemplate { items };
    Html(template.render().unwrap())
}

#[axum_macros::debug_handler]
async fn add_item(
    State(pool): State<PgPool>,
    Form(input): Form<ItemForm>,
) -> axum::response::Html<String> {
    item::add_item(input, &pool).await.unwrap();
    let items = item::list_items(&pool).await.unwrap();
    let template = IndexTemplate { items };
    Html(template.render().unwrap())
}

#[derive(Template)]
#[template(path = "item.html")]
struct ItemTemplate {
    item: item::Item,
}

async fn get_item(Path(id): Path<i32>, State(pool): State<PgPool>) -> axum::response::Html<String> {
    let item = item::get_item_by_id(id, &pool).await.unwrap();
    // TODO allow the option to get an item by name
    let template = ItemTemplate { item };
    Html(template.render().unwrap())
}

async fn delete_item(Path(id): Path<i32>, State(pool): State<PgPool>) {
    item::delete_item(id, &pool).await.unwrap();
    println!("deleting item {}", id);
}
