DROP TABLE IF EXISTS items;
CREATE TABLE items (id serial primary key not null, name varchar, description text);
